﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot
{
    public class YoutubeInterface
    {
        private YouTubeService youtube;

        public YoutubeInterface(string apiKey)
        {
            youtube = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = apiKey, //Move this to file; it's secret info that shouldn't be part of source code
                ApplicationName = this.GetType().ToString()
            });
        }

        private async Task<string> getChannelListResponseAsync(string channelId) //This can probably be cached and updated infrequently
        {
            var channelListRequest = youtube.Channels.List("contentDetails");
            channelListRequest.Id = channelId;
            channelListRequest.MaxResults = 50;

            ChannelListResponse channelListResponse = await channelListRequest.ExecuteAsync();
            return channelListResponse.Items[0].ContentDetails.RelatedPlaylists.Uploads.ToString();
        }

        public async Task<List<string>> getVideoListAsync(string channelId) //This can cache the results for its import for an hour or longer
        {
            var videoListRequest = youtube.PlaylistItems.List("snippet");
            videoListRequest.PlaylistId = await getChannelListResponseAsync(channelId);
            videoListRequest.MaxResults = 50;


            PlaylistItemListResponse videoListResponse = videoListRequest.Execute();
            var videoList = new List<string>();
            foreach (var playlistItem in videoListResponse.Items)
            {
                videoList.Add(playlistItem.Snippet.ResourceId.VideoId);
            }
            return videoList;
        }

        public async Task<string> searchForChannel(string channelTitle)
        {
            var searchRequest = youtube.Search.List("snippet");
            searchRequest.Type = "channel";
            searchRequest.Q = channelTitle;

            //Hope the first result is the right channel
            SearchListResponse searchListResponse = await searchRequest.ExecuteAsync();
            if (searchListResponse.Items.Count > 0)
            {
                return searchListResponse.Items[0].Snippet.ChannelId;
            }
            return null;
        }
    }
}
