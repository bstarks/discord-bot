using System;

namespace DiscordBot.Dependencies {
    class MarkovUnit {
        
        public static MarkovUnit START = new MarkovUnit("");
        public static MarkovUnit END = new MarkovUnit("");

        public string word {get; private set;}

        public MarkovUnit(string value) {
            this.word = value;
        }

        public override bool Equals(Object input) {
            if (!input.GetType().Equals(this.GetType())) {
                return false;
            }
            MarkovUnit unit = input as MarkovUnit;
            if (this == unit) {
                return true;
            } else {
                return this.word == unit.word; 
            }
        }

        public override int GetHashCode() {
            return word.GetHashCode();
        }


    }
}