using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DiscordBot.Dependencies {
    class MarkovModel {
        private Dictionary<TransitionKey, List<TransitionValue>> transitions;
        private int order;

        public MarkovModel(int order = 1) {
            this.order = order;
            transitions = new Dictionary<TransitionKey, List<TransitionValue>>();
        }

        public void buildModelFromMessage(List<string> messages) {
            var wordListList = messages.Select(x => x.Split(' '));
            var unitQueue = new FixedSizedQueue<MarkovUnit>(order);
            foreach (var words in wordListList) {
                MarkovUnit startValueUnit = addStartWord(words[0]).unit;
                unitQueue.Enqueue(MarkovUnit.START);
                unitQueue.Enqueue(startValueUnit);
                TransitionKey currentKey = null;
                MarkovUnit currentValueUnit = null;
                for (int i = 1; i <= words.Count() - 1; i++) {
                    currentKey = createKey(unitQueue.ToArray());
                    currentValueUnit = addTransitionForKey(currentKey, words[i]).unit;
                    unitQueue.Enqueue(currentValueUnit);
                }
                addEndWord(words[words.Count() -1]);
            }
        }

        public string generateRandomText(int maxLength = 500) {
            TransitionValue value = getValue(TransitionKey.START);
            if (value == null) {
                return null;
            }
            var sb = new StringBuilder();
            var unitQueue = new FixedSizedQueue<MarkovUnit>(order);
            unitQueue.Enqueue(MarkovUnit.START);

            MarkovUnit currentUnit  = null;
            TransitionKey currentKey = TransitionKey.START;
            while (currentUnit != MarkovUnit.END) {
                value = getValue(currentKey);
                sb.Append(value.unit.word + " ");
                if (sb.Length > maxLength) {
                    sb.Append("...");
                    break;
                }

                currentUnit = value.unit;
                unitQueue.Enqueue(currentUnit);
                currentKey = createKey(unitQueue.ToArray());
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        private TransitionValue addStartWord(string startWord) {
            return addTransitionForKey(TransitionKey.START, startWord);
        }

        private void addEndWord(string end) {
            TransitionKey endKey = createKey(new MarkovUnit(end));
            TransitionValue endValue = getEndValue(endKey);
            endValue.increment();
        }

        private TransitionValue addTransitionForKey(TransitionKey key, string to) {
            List<TransitionValue> values = transitions.GetOrAdd(key, (_) => new List<TransitionValue>());
            TransitionValue value = getSingleValueOrAddValue(values, to);
            value.increment();
            return value;
        }

        private TransitionValue getValue(TransitionKey key) {
            List<TransitionValue> values = transitions.GetValueOrDefault(key);
            if (values ==  null) {
                return null;
            }
            int totalTransitions = values.Select(x => x.numOfTransitions).Sum();
            int randomChoice = new Random().Next(totalTransitions);
            TransitionValue selectedValue = null;
            values.Aggregate(randomChoice, (count, currentValue) => { //This is O(n), replace it later maybe?
                int futureCount = count - currentValue.numOfTransitions;
                if (count >= 0 && futureCount < 0) {
                    selectedValue = currentValue;
                }
                return futureCount;
            });
            return selectedValue;
        }

        private TransitionValue getEndValue(TransitionKey key) {
            List<TransitionValue> values = transitions.GetOrAdd(key, (_) => new List<TransitionValue>());
            TransitionValue value = values.SingleOrDefault( v => v.unit == MarkovUnit.END);
            if (value == null) {
                value = new TransitionValue(MarkovUnit.END);
                values.Add(value);
            }
            return value;
        }

        private TransitionValue getSingleValueOrAddValue(List<TransitionValue> values, string valueToGet) {
            TransitionValue value = values.SingleOrDefault( v => v.unit.word == valueToGet);
            if (value == null) {
                value = new TransitionValue(new MarkovUnit(valueToGet));
                values.Add(value);
            }
            return value;
        }

        private TransitionKey createKey(params MarkovUnit[] units) {
            return new TransitionKey(units);
        }
    }
}