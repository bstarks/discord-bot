using System.Collections.Generic;

class FixedSizedQueue<T>  {

    private int size;
    private Queue<T> queue;

    public FixedSizedQueue(int size) {
        this.size = size;
        queue = new Queue<T>();
    }

    public void Enqueue(T obj) {
        queue.Enqueue(obj);
        if (queue.Count > size) {
            queue.Dequeue();
        }
    }

    public T Dequeue() {
        return queue.Dequeue();
    }

    public T[] ToArray() {
        return queue.ToArray();
    }

}