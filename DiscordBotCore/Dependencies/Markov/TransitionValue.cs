namespace DiscordBot.Dependencies {
    class TransitionValue {
        public MarkovUnit unit {get; private set;}
        public int numOfTransitions {get; private set;}

        public TransitionValue(MarkovUnit value) {
            this.unit = value;
            numOfTransitions = 0;
        }

        public void increment() {
            numOfTransitions++;
        }
    }
}