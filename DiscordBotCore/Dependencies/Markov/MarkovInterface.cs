using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Discord.Commands;
using Discord;

namespace DiscordBot.Dependencies {

    public class MarkovInterface {
        private Dictionary<int, MarkovModel> userIdToMarkovModelDictionary;

        public MarkovInterface() {
            userIdToMarkovModelDictionary = new Dictionary<int, MarkovModel>();
        }

        public async Task<string> getMarkovGeneratedTextForUser(string username, ICommandContext context, int length = 5) {
            var model = await getMarkovModelForUser(username, context);
            if (model == null) {
                return null;
            }
            return model.generateRandomText();
        }

        private async Task<MarkovModel> getMarkovModelForUser(string username, ICommandContext context) {
            //Look these up in some database later (make async)
            //For now, just build a new markov model
            var model = new MarkovModel();
            var messages = await getMessagesForUser(username, context);
            messages = messages.Where(x => x.Count() > 0 && x[0] != Bot.COMMAND_PREFIX).ToList();
            model.buildModelFromMessage(messages);
            return model;
        }

        private async Task<List<string>> getMessagesForUser(string username, ICommandContext context) {
            var lowerUsername = username.ToLower();
            var user = (await context.Channel.GetUsersAsync().Flatten()).FirstOrDefault(u => u.Username.ToLower() == lowerUsername);
            if (user != null) {
                return await getMessagesForUser(user.Id, context);
            } else {
                return null;
            }
        }

        private async Task<List<string>> getMessagesForUser(ulong userId, ICommandContext context) {
            var messages = await context.Channel.GetMessagesAsync().Flatten();
            return messages.Where(msg => msg.Author.Id == userId).Select(msg => msg.Content).ToList();
        }
    }
}