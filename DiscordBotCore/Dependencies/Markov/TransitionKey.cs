using System;
using System.Linq;

namespace DiscordBot.Dependencies {
    class TransitionKey {

        public static TransitionKey START = new TransitionKey(MarkovUnit.START);

        public MarkovUnit[] units {get; private set;}
        
        public TransitionKey(params MarkovUnit[] units) {
            this.units = units;
        }

        public bool contains(string str) {
            if (str == "") {
                return false;
            }
            return units.Any(unit => unit.word == str);
        }

        public override bool Equals(Object input) {
            if (!input.GetType().Equals(this.GetType())) {
                return false;
            }
            TransitionKey key = input as TransitionKey;

            if (this.units.Length != key.units.Length) {
                return false;
            }
            for (int i  = 0; i < units.Length; i++) {
                if (!units[i].Equals(key.units[i])) {
                    return false;
                }
            }
            return true;
        }

        public override int GetHashCode() {
            return units.Select(x => x.GetHashCode()).Aggregate((x,y) => x ^ y);
        }
    }
}