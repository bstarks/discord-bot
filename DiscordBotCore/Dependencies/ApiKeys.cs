﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Dependencies
{
    public class ApiKeys
    {
        public string discordApiKey;
        public string youtubeApiKey;
        public string giphyApiKey;
    }
}
