﻿using DiscordBot.Modules;
using GiphyDotNet.Manager;
using GiphyDotNet.Model.Parameters;
using GiphyDotNet.Model.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Dependencies
{
    public class GiphyInterface
    {
        private Giphy giphy;

        public GiphyInterface(string apiKey)
        {
            giphy = new Giphy(apiKey);
        }

        public async Task<GiphySearchResult> getGifsForSearchQuery(string query)
        {
            var searchParameter = new SearchParameter()
            {
                Query = query
            };
            return await giphy.GifSearch(searchParameter);
        }


    }
}
