﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Google.Apis.YouTube.v3;
using Discord;
using System.Threading;

namespace DiscordBot
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().MainAsync().GetAwaiter().GetResult();
        }

        public async Task MainAsync()
        {
            Bot bot = new Bot();
            Console.CancelKeyPress += async delegate {
                await bot.DisconnectAsync();
                Environment.Exit(0);
            };
            await bot.Initialize();
            await bot.ConnectAsync();

            Thread.Sleep(Timeout.Infinite);
        }
    }
}
