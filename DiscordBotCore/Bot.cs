﻿using Discord;
using Discord.Commands;
using Discord.Net.Providers.WS4Net;
using Discord.WebSocket;
using DiscordBot.Dependencies;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.IO;

namespace DiscordBot
{
    public class Bot
    {
        private DiscordSocketClient discordClient;
        private CommandService commandService;
        private YoutubeInterface youtubeInterface;
        private GiphyInterface giphyInterface;
        private IServiceProvider serviceProvider;
        private MarkovInterface markovInterface;
        private ApiKeys apiKeys;

        public const char COMMAND_PREFIX = '!';
        private const string UNKNOWN_COMMAND_MSG_FORMAT = "What the FUCK is {0}?!";
        private const string UNKNOWN_COMMAND_MSG = "What the fuck?!";


        public Bot()
        {
            string jsonFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configuration");
            jsonFilePath = Path.Combine(jsonFilePath, "apiKeys.json");
            string apiKeysJson;

            try {
                apiKeysJson = new StreamReader(jsonFilePath).ReadToEnd();
            }
            catch (DirectoryNotFoundException e) {
                throw handleMissingFileException(e, jsonFilePath);
            }
            catch (FileNotFoundException e) {
                throw handleMissingFileException(e, jsonFilePath);
            }
            
            apiKeys = JsonConvert.DeserializeObject<ApiKeys>(apiKeysJson);

            discordClient = new DiscordSocketClient(new DiscordSocketConfig
            {
                WebSocketProvider = WS4NetProvider.Instance
            });

            youtubeInterface = new YoutubeInterface(apiKeys.youtubeApiKey);

            giphyInterface = new GiphyInterface(apiKeys.giphyApiKey);

            markovInterface = new MarkovInterface();

            commandService = new CommandService();
        }

        public async Task Initialize()
        {
            discordClient.Log += Log;
            discordClient.MessageReceived += MessageReceivedHandlerAsync;

            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton(youtubeInterface);
            serviceCollection.AddSingleton(commandService);
            serviceCollection.AddSingleton(giphyInterface);
            serviceCollection.AddSingleton(markovInterface);
            serviceProvider = serviceCollection.BuildServiceProvider();

            await commandService.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        public async Task ConnectAsync()
        {
            await discordClient.LoginAsync(TokenType.Bot, apiKeys.discordApiKey);
            await discordClient.StartAsync();
        }

        public bool isConnected()
        {
            return discordClient.ConnectionState == ConnectionState.Connected;
        }

        public async Task DisconnectAsync()
        {
            await discordClient.LogoutAsync();
            await discordClient.StopAsync();
        }

        private async Task MessageReceivedHandlerAsync(SocketMessage socketMessage)
        {
            SocketUserMessage userMessage = socketMessage as SocketUserMessage;
            if (userMessage == null || userMessage.Author.IsBot)
            {
                return;
            }

            int argPos = 0;
            if (!userMessage.HasCharPrefix(COMMAND_PREFIX, ref argPos))
            {
                return;
            }

            var context = new CommandContext(discordClient, userMessage);
            var result = await commandService.ExecuteAsync(context, argPos, serviceProvider);
            if (!result.IsSuccess)
            {
                await handleError(result, userMessage, context);
            }
        }

        private async Task handleError(IResult result, IUserMessage userMessage, ICommandContext context)
        {
            switch (result.Error)
            {
                case CommandError.UnknownCommand:
                    string errorMsg = userMessage.Content.Length > 1 ?
                        String.Format(UNKNOWN_COMMAND_MSG_FORMAT, userMessage.Content.Substring(1)) : UNKNOWN_COMMAND_MSG;
                    await context.Channel.SendMessageAsync(errorMsg);
                    break;
                case CommandError.Exception:
                    
                default:
                    await context.Channel.SendMessageAsync(result.ErrorReason);
                    break;
            }
        }

        private FileNotFoundException handleMissingFileException(Exception e, String filePath) {
            var msg = "Can not found API keys JSON file at " + filePath;
            var logMsg = new LogMessage(LogSeverity.Critical, "Bot.cs", msg, e);
            Log(logMsg);
            return new FileNotFoundException(msg);
        }

        private Task Log(LogMessage log)
        {
            Console.WriteLine(log.ToString());
            return Task.Delay(0);
        }
    }
}
