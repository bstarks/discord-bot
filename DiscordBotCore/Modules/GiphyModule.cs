﻿using Discord.Commands;
using DiscordBot.Dependencies;
using GiphyDotNet.Model.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Modules
{
    public class GiphyModule : ModuleBase
    {
        private GiphyInterface giphyInterface;

        public GiphyModule(GiphyInterface giphyInterface)
        {
            this.giphyInterface = giphyInterface;
        }

        [Command("giphy"), Alias("gif"), Summary("Fetch a GIF from giphy")]
        public async Task giphy(params string[] query)
        {
            string fullQuery = string.Join(" ", query);
            var giphyResult = await giphyInterface.getGifsForSearchQuery(fullQuery);
            if (giphyResult.Data.Length > 0) {
                await ReplyAsync(giphyResult.Data[0].Url);
            }
            else
            {
                await ReplyAsync("Apparently there aren't any gifs that match " + fullQuery);
            }
        }
    }
}
