﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Modules
{
    public class YoutubeModule : ModuleBase
    {
        private YoutubeInterface youtubeInterface;

        private const string GIIVA_YOUTUBE_ID = "UC9ecwl3FTG66jIKA9JRDtmg";
        private const string YOUTUBE_VIDEO_PRELUDE = "https://www.youtube.com/watch?v=";

        public YoutubeModule(YoutubeInterface youtubeInterface)
        {
            this.youtubeInterface = youtubeInterface;
        }

        [Command("giiva"), Alias("siiva"), Summary("Get a video")]
        public async Task giiva()
        {
            await getRandomVideoFromYoutubeChannel(GIIVA_YOUTUBE_ID);
        }

        [Command("youtube"), Alias("y"), Summary("Get a video from a youtube channel")]
        public async Task youtube(params string[] channelNameParams)
        {
            string channelName = string.Join(" ", channelNameParams);
            string channelId = await youtubeInterface.searchForChannel(channelName);
            await getRandomVideoFromYoutubeChannel(channelId);
            
        }

        private async Task getRandomVideoFromYoutubeChannel(String channelId)
        {
            List<string> videos = await youtubeInterface.getVideoListAsync(channelId);
            int randomIndex = new Random().Next(videos.Count);
            string randomVideoId = videos[randomIndex];
            await ReplyAsync(YOUTUBE_VIDEO_PRELUDE + randomVideoId);
        }
    }
}
