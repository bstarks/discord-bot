﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Modules
{
    public class CommandModule : ModuleBase
    {
        private CommandService commandService;

        public CommandModule(CommandService commandService)
        {
            this.commandService = commandService;
        }

        [Command("commands"), Alias("help"), Summary("Get a list of commands")]
        public async Task commands()
        {
            var commands = commandService.Commands;
            var sb = new StringBuilder();
            sb.AppendFormat("List of commands: \n\n");
            foreach(var commandInfo in commands)
            {
                sb.AppendFormat(printCommand(commandInfo) + "\n");
            }
            await ReplyAsync(sb.ToString());
        }

        private string printCommand(CommandInfo commandInfo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(commandInfo.Name);
            if (commandInfo.Parameters.Count == 0)
            {
                sb.Append(": ");
            }
            else
            {
                sb.Append(" " + printParams(commandInfo.Parameters) + " : ");
            }
            sb.Append(commandInfo.Summary);
            return sb.ToString();
        }

        private string printParams(IReadOnlyList<ParameterInfo> parameters)
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < parameters.Count; i++) {
                ParameterInfo parameter = parameters[i];
                sb.Append("<");
                sb.Append(parameter.Name);
                sb.Append(">");
                if (i != (parameters.Count - 1))
                {
                    sb.Append(" ");
                }
            }
            return sb.ToString();
        }
    }
}
