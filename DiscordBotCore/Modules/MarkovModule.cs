using Discord;
using Discord.WebSocket;
using Discord.Commands;
using DiscordBot.Dependencies;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace DiscordBot.Modules{
    public class MarkovModule : ModuleBase {
        private MarkovInterface markovInterface;
        public MarkovModule(MarkovInterface markovInterface) {
            this.markovInterface = markovInterface;
        }

        [Command("markov"), Summary("Generate random text based on user's previous posts")]
        public async Task markov(params string[] userNameParams) {
            string userName = string.Join(" ", userNameParams);
            string message = await markovInterface.getMarkovGeneratedTextForUser(userName, Context);
            if (message != null) {
                await ReplyAsync(message);
            } else {
                await ReplyAsync("No messages found for " + userName + " (whoever that is) (commands are ignored).");
            }
        }
    }
}